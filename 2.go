package main

import "fmt"
import "strings"
import "unicode"


func main (){

	fmt.Println(conv("SuperFunctionBody"))
}

func conv (s string) string {
	res := ""
	if len(s) == 0 {return s}
	for i, r := range s {
		
		if unicode.IsUpper(r) && unicode.IsLetter(r) {
			if i != 0 {
			res += "_" + strings.ToLower(string(r))
			 
			}else {
			res += strings.ToLower(string(r))
			}
			
	    } else {
			res += string(r)
			}
	}
	return res
}
