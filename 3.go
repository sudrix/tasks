package main


import "time"
import "log"
import "errors"


func load (sig <-chan int, c int) (bool,error) {
	rps  := 1/c
	select {
            case <- sig:
				log.Println("OK")
				return true, nil 
            case <-time.After(time.Duration(rps)*time.Second):
				log.Println("Custom err")
                return false, errors.New("error")
    }
	
}

func ticker (sig chan<- int, c int) {
	rps  := 1/c
	for {
		time.Sleep(time.Duration(rps)*time.Second)
		sig <- 1
	}
}

func main (){

	s := make(chan int)
	go ticker(s,1)

for i:=0; i<100; i++ {
	time.Sleep(time.Millisecond*500)
	go load(s,1)
	if i==100 {close(s)}
}



}